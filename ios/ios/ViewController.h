//
//  ViewController.h
//  ios
//
//  Created by Martin Kunev on 8.4.14.
//  Copyright (c) 2014 Sofia university. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
{
	IBOutlet UITextField *device_name_text;
	IBOutlet UITextField *email_text;
	IBOutlet UITextField *device_password_text;

	IBOutlet UIButton *connect;
}
@end
