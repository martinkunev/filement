int processDLNAGET(struct http_request *restrict request, struct http_response *restrict response, struct resources *restrict resources);
int processDLNAPOST(struct http_request *restrict request, struct http_response *restrict response, struct resources *restrict resources);
int processDLNASUBSCRIBE(struct http_request *restrict request, struct http_response *restrict response, struct resources *restrict resources);
int processDLNANOTIFY(struct http_request *restrict request, struct http_response *restrict response, struct resources *restrict resources);
int processDLNArequest(struct http_request *restrict request, struct http_response *restrict response, struct resources *restrict resources);
